// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MyProject333GameMode.generated.h"

UCLASS(minimalapi)
class AMyProject333GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AMyProject333GameMode();
};



