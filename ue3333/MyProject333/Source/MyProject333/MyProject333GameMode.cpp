// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "MyProject333GameMode.h"
#include "MyProject333HUD.h"
#include "MyProject333Character.h"
#include "UObject/ConstructorHelpers.h"

AMyProject333GameMode::AMyProject333GameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AMyProject333HUD::StaticClass();
}
